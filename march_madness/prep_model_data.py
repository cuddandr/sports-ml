#!/mnt/home/cuddandr/usr/bin/python3

import numpy as np
import pandas as pd

print("Loading CSV files...")

df_adv_stats = pd.read_csv("reg_season_adv_stats_2019.csv")
df_ncaa = pd.read_csv("ncaa_tournament_results.csv")
df_seeds = pd.read_csv("./input/2019/NCAATourneySeeds.csv")
df_rank = pd.read_csv("./input/2019/MasseyOrdinals.csv")
print(df_ncaa.head(10))

df_features = df_adv_stats[['Season', 'TeamID', 'shoot_eff', 'score_op', 'off_rtg', 'def_rtg', 'sos', 'ie', 'efg_pct', 'to_poss', 'orb_pct', 'drb_pct', 'ft_rate', 'ts_pct', 'ast_rtio']]

df_seeds['seed'] = df_seeds['Seed'].apply(lambda x : int(x[1:3]))
df_seeds = df_seeds[['Season', 'TeamID', 'seed']]
df_seeds = df_seeds[df_seeds.Season >= 2003]
print(df_seeds.head())

df_rank = df_rank[(df_rank.RankingDayNum == 133)]
df_rank.drop('RankingDayNum', inplace=True, axis=1)

df_rank = pd.pivot_table(df_rank, index=['Season', 'TeamID'], columns='SystemName', values='OrdinalRank')
df_rank.reset_index(inplace=True)
df_rank = df_rank[['Season', 'TeamID', 'POM', 'SAG']]
df_rank.to_csv("ranking.csv", index=False)
print(df_rank.head(10))

df_features = pd.merge(df_rank, df_features, how='left', left_on=['Season', 'TeamID'], right_on=['Season', 'TeamID'])
df_features = pd.merge(df_seeds, df_features, how='left', left_on=['Season', 'TeamID'], right_on=['Season', 'TeamID'])
print(df_features.head(10))
df_features.to_csv("feature_data.csv", index=False)

#X = pd.merge(left=df_ncaa[['Season','WTeamID','Wseed','LTeamID','Lseed']], right=df_features, how='left', left_on=['Season', 'WTeamID'], right_on=['Season', 'TeamID'])
#X = pd.merge(left=X, right=df_features, how='left', suffixes=['_w', '_l'], left_on=['Season', 'LTeamID'], right_on=['Season', 'TeamID'])
#print(X.head())

df_model = pd.DataFrame(columns=['seed', 'POM', 'SAG', 'shoot_eff', 'score_op', 'off_rtg', 'def_rtg', 'sos', 'ie', 'efg_pct', 'to_poss', 'orb_pct', 'drb_pct', 'ft_rate', 'ts_pct', 'ast_rtio', 'season', 'team1', 'team2', 'result', 'upset'])
for (index_label, row_series) in df_ncaa.iterrows():
    season = row_series['Season']
    team1 = np.minimum(row_series['WTeamID'], row_series['LTeamID'])
    team2 = np.maximum(row_series['WTeamID'], row_series['LTeamID'])
    #print("Team1: {}; Team2: {}".format(team1, team2))

    team1_stat = df_features[(df_features.TeamID == team1) & (df_features.Season == season)]
    team2_stat = df_features[(df_features.TeamID == team2) & (df_features.Season == season)]

    team1_stat.drop(['Season','TeamID'], inplace=True, axis=1)
    team2_stat.drop(['Season','TeamID'], inplace=True, axis=1)

    team1_stat.reset_index(inplace=True, drop=True)
    team2_stat.reset_index(inplace=True, drop=True)
    stat_join = pd.merge(left=team1_stat, right=team2_stat, how='inner', left_index=True, right_index=True, suffixes=['_1', '_2'])
    stat_diff = team1_stat - team2_stat
    #stat_diff = team1_stat.sub(team2_stat, fill_value=0)

    seed_diff = stat_diff.iloc[0]['seed']
    stat_diff['season'] = season
    stat_diff['team1'] = team1
    stat_diff['team2'] = team2

    stat_join['season'] = season
    stat_join['team1'] = team1
    stat_join['team2'] = team2

    if seed_diff < 0:
        if team1 == row_series['WTeamID']:
            stat_diff['result'] = 1
            stat_diff['upset'] = 0
        else:
            stat_diff['result'] = 0
            stat_diff['upset'] = 1

    elif seed_diff > 0:
        if team1 == row_series['WTeamID']:
            stat_diff['result'] = 1
            stat_diff['upset'] = 1
        else:
            stat_diff['result'] = 0
            stat_diff['upset'] = 0

    else:
        if team1 == row_series['WTeamID']:
            stat_diff['result'] = 1
            stat_diff['upset'] = 0
        else:
            stat_diff['result'] = 0
            stat_diff['upset'] = 0

    df_model = df_model.append(stat_diff, ignore_index=True)

    #print(team1_stat)
    #print(team2_stat)
    #print(stat_diff)

print(df_model.head(10))
df_model.to_csv("model_data.csv",index=False)
