#!/mnt/home/cuddandr/usr/bin/python3

import os
os.environ['OPENBLAS_NUM_THREADS'] = '12'

import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.linear_model import LogisticRegression

from sklearn.neural_network import MLPClassifier

from sklearn.ensemble import AdaBoostClassifier
from sklearn.ensemble import RandomForestClassifier

print("Loading CSV files...")
df_model = pd.read_csv("model_data.csv")
print(df_model.head(10))
#df_model.drop(['shoot_eff', 'score_op', 'ast_rtio'], inplace=True, axis=1)
#df_model.drop(['seed'], inplace=True, axis=1)

X = df_model.iloc[:, :-5]
y = df_model.iloc[:, -2]

print(X.head(10))
print(y.head(10))

X_num = X.values
y_num = y.values

X_train, X_test, y_train, y_test = train_test_split(X_num, y_num, test_size=0.25, random_state=32)
scoring_str = 'neg_log_loss'

logreg = LogisticRegression(solver='liblinear', max_iter=5000)
params = {'C' : np.logspace(start=-4, stop=4, num=15), 'fit_intercept' : [True, False]}
clf = GridSearchCV(logreg, params, scoring=scoring_str, refit=True, cv=5)

#nnet = MLPClassifier(solver='sgd', max_iter=1000, activation='logistic')
#params = {'alpha' : np.logspace(start=-4, stop=3, num=13), 'hidden_layer_sizes' : [(10,),(5,5),(10,5)]}
#clf = GridSearchCV(nnet, params, scoring=scoring_str, refit=True, cv=5)

#adaboost = AdaBoostClassifier()
#params = {'learning_rate' : np.logspace(start=-4, stop=1, num=10), 'n_estimators' : [30,40,50,70,90,120]}
#clf = GridSearchCV(adaboost, params, scoring=scoring_str, refit=True, cv=5)

#rngforest = RandomForestClassifier()
#params = {'n_estimators' : [50,70,90,110,130], 'criterion' : ['gini', 'entropy'], 'min_samples_leaf' : [2,4,8,16]}
#clf = GridSearchCV(rngforest, params, scoring=scoring_str, refit=True, cv=5)

#clf.fit(X_train, y_train)
#predicted = clf.predict(X_test)
#probabilities = clf.predict_proba(X_test)

#train_accuracy = clf.score(X_train, y_train)
#test_accuracy = clf.score(X_test, y_test)

#print("Training Accuracy: {:0.4}".format(train_accuracy))
#print("Test Accuracy: {:0.4}\n".format(test_accuracy))
#print(clf.best_params_)

#print(predicted)
#print(probabilities)

clf.fit(X_num, y_num)

df_features = pd.read_csv("./feature_data.csv")
df_sub = pd.read_csv("./input/SampleSubmissionStage2.csv")

for row in df_sub.itertuples():
    game_id = row.ID
    game_id = [int(x) for x in game_id.split('_')]

    team1 = df_features[(df_features.Season == game_id[0]) & (df_features.TeamID == game_id[1])]
    team2 = df_features[(df_features.Season == game_id[0]) & (df_features.TeamID == game_id[2])]
    #print(team1)
    #print(team2)

    team1 = team1.drop(['Season','TeamID'], axis=1).copy()
    team2 = team2.drop(['Season','TeamID'], axis=1).copy()

    team1.reset_index(inplace=True, drop=True)
    team2.reset_index(inplace=True, drop=True)
    X_game = team1 - team2
    X_vals = X_game.values
    X_prob = clf.predict_proba(X_vals)

    df_sub.at[row.Index, 'Pred'] = X_prob[0][1]
    #print(game_id)
    #print(X_game)
    #print(X_prob[0][0])

df_sub.to_csv("sample_pred.csv", index=False)
